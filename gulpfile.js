var elixir = require('laravel-elixir');

elixir.config.assetsPath = 'resources/assets/app/assets';
//elixir.config.css.sass.folder = 'public/app/assets';

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'public/app/assets/css');

    mix.copy('resources/assets/app', 'public/app');
});