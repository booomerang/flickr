
var FlickrApp = angular.module('FlickrApp', ['ngRoute', 'ui.bootstrap']);

FlickrApp.config(function($routeProvider, $locationProvider){
    $routeProvider
        .when('/', {
            controller: 'indexController',
            templateUrl: 'views/index.html'
        })
        .when('/image/:imageId', {
            controller: 'imageDetailController',
            templateUrl: 'views/image-detail.html'
        })
        .otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode(true);
});

FlickrApp.filter('paginate', function(){
    return function(data, from){
        return data.slice(from);
    };
});