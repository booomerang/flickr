
FlickrApp.controller('indexController', ['$scope', '$http', '$log', function($scope, $http, $log) {
    $scope.images = {};
    $scope.images.items = [];

    $scope.currentPage = 1;
    $scope.perPage = 5;

    $scope.loading = false;

    $scope.images.search = function() {
        $scope.images.items = [];
        $scope.loading = true;
        $http.get('/api.php', {params: {q: $scope.images.search_input}})
        .then(function(response){
            $scope.images.items = response.data;
            $scope.loading = false;
        }, function(response){
            //console.log(response);
        });
    };

    $scope.pageChanged = function() {
        console.log('Page changed to: ' + $scope.currentPage);
    };
}]);