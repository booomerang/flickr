
FlickrApp.controller('imageDetailController', ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
    $scope.imageId = $routeParams.imageId;
    $scope.image = {};

    $scope.loading = false;

    $scope.loading = true;
    $http.get('/api.php', {params: {id: $scope.imageId}})
        .then(function(response){
            console.log(response.data);
            $scope.image.info = response.data;
            $scope.loading = false;
        }, function(response){
            //console.log(response);
        });
}]);