<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiController extends Controller
{
    public function index(Request $request)
    {
        $q = $request->input('q');
        $id = $request->input('id');

        if (isset($q) && is_null($id)) {
            $result = $this->getPhotos($q);
        } elseif (isset($id) && is_null($q)) {
            $result = $this->getPhotoInfo($id);
        }

        return $result;
    }

    protected function getPhotos($q)
    {
        $url = 'https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=907dea022b0dfc2ec993df236bc0fe6c'
            . '&text='.$q
            . '&format=json&nojsoncallback=1';
        $data = file_get_contents($url);

        $photos = [];
        if ($data !== false) {
            $returnedData = json_decode($data, true);

            $photos = $returnedData['photos']['photo'];

            //$imgUrl = 'https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg';
            if (isset($photos)) {
                foreach($photos as $key => $photo) {
                    $photos[$key]['image']['small'] = "https://farm{$photo['farm']}.staticflickr.com/{$photo['server']}/{$photo['id']}_{$photo['secret']}_m.jpg";
                    $photos[$key]['image']['big'] = "https://farm{$photo['farm']}.staticflickr.com/{$photo['server']}/{$photo['id']}_{$photo['secret']}_b.jpg";
                }
            }

            //var_dump($photos); die;
            //var_dump($returnedData['photos']['photo']); die;
        }

        return $photos;
    }

    protected function getPhotoInfo($id)
    {
        $url = 'https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key=907dea022b0dfc2ec993df236bc0fe6c'
                .'&photo_id='.$id
                .'&format=json&nojsoncallback=1';
        $data = file_get_contents($url);

        $photo = [];
        if ($data !== false) {
            $photo = json_decode($data, true);

            //var_dump($photo); die;

            $photo['photo']['image'] = [];
            $photo['photo']['image']['big'] = "https://farm{$photo['photo']['farm']}.staticflickr.com/{$photo['photo']['server']}/{$photo['photo']['id']}_{$photo['photo']['secret']}_b.jpg";
        }

        return $photo;
    }
}